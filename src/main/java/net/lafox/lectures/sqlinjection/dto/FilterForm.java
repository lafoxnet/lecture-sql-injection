package net.lafox.lectures.sqlinjection.dto;

public class FilterForm {
    private String filter;

    public FilterForm() {
    }

    public FilterForm(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
