package net.lafox.lectures.sqlinjection.mvc;

import net.lafox.lectures.sqlinjection.dao.ItemDao;
import net.lafox.lectures.sqlinjection.dao.UserDao;
import net.lafox.lectures.sqlinjection.dto.AddForm;
import net.lafox.lectures.sqlinjection.dto.FilterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Transactional
public class HtmlController {

    @Autowired
    private ItemDao itemDao;
    @Autowired
    private UserDao userDao;


    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("list", itemDao.findItems());
        model.addAttribute("filterForm", new FilterForm());
        return "index";
    }

    @GetMapping("item/{id}")
    public String item(Model model, @PathVariable Integer id) {
        model.addAttribute("item", itemDao.findById(id));
        return "item";
    }

    @PostMapping("filter")
    public String filter(@ModelAttribute("filterForm") FilterForm form, BindingResult result, Model model) {
        model.addAttribute("list", itemDao.findByText(form.getFilter()));
        model.addAttribute("filterForm", new FilterForm(form.getFilter()));
        return "index";
    }


    @GetMapping("createNew")
    public String createNew(Model model) {
        model.addAttribute("addForm", new AddForm());
        return "createNew";
    }

    @PostMapping("createNew")
    public String createNew(@ModelAttribute("addForm") AddForm form, BindingResult result, Model model) {
        boolean validUser = userDao.isValidUser(form.getUsername(), form.getPassword());
        if(validUser){
            itemDao.add(form.getTitle(), form.getBody());
        }
        return "createNew";
    }



}

