package net.lafox.lectures.sqlinjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LectureSQLInjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(LectureSQLInjectApplication.class, args);
	}
}
