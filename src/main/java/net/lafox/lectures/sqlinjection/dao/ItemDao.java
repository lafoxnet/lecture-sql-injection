package net.lafox.lectures.sqlinjection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import net.lafox.lectures.sqlinjection.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class ItemDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final ItemMapper itemMapper = new ItemMapper();

    public Item findById(Integer id) {
        return jdbcTemplate.queryForObject("select id, title, body  from blog where id =  ?",  itemMapper, 1);
    }

    public List<Item> findByText(String text) {
        return jdbcTemplate.query("select id, title, body from blog where title like '%"+text+"%'", itemMapper);
        // ' union select id , name || '-' || password as title, password as body from users --
//        return jdbcTemplate.query("select id, title, body from blog where title  = '?'", itemMapper, text);
    }

    public List<Item>  findItems() {
        return jdbcTemplate.query("select id, title, body  from blog ",  itemMapper);
    }

    public void add(String title, String body) {
         jdbcTemplate.update("insert into blog (title, body) VALUES (?,?)",  title, body);

    }

    private static class ItemMapper implements RowMapper<Item> {
        @Override
        public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Item(rs.getInt("id"), rs.getString("title"), rs.getString("body"));
        }
    }
}
