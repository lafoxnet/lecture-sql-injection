package net.lafox.lectures.sqlinjection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean isValidUser(String username, String password) {
        return jdbcTemplate.queryForObject("select count(*)=1 as isValid  from users where name=? and password =?", new BooleanMapper(), username, password );
//        return jdbcTemplate.queryForObject("select id, title, body  from blog where id = 1 union all insert into blog (title, body) VALUES ('Bla','Bla');",  itemMapper);
//        return jdbcTemplate.queryForObject("select id,msg as title  from t where id = ? ", new Object[]{id},  itemMapper);
    }


   private static class BooleanMapper implements RowMapper<Boolean> {
        @Override
        public Boolean mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getBoolean("isValid");
        }
    }
}
