<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html><body>

<h1>Blog</h1>

<UL>
<c:forEach items="${list}" var="item">
   <li><a href="item/${item.id}">${item.title}</a></li>
</c:forEach>
</UL>
<hr/>
<form:form  action="filter" modelAttribute="filterForm" method="POST">
   <form:input path="filter" size="60"/>
</form:form>

<hr/>

<a href="createNew" >Create new</a>
</body></html>