<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html><body>
<h1><a href="/">Blog</a>: Create New</h1>


<form:form  action="createNew" modelAttribute="addForm" method="POST">
   title: <form:input path="title"/><br/>
   body: <form:input path="body"/><br/>
   username: <form:input path="username"/><br/>
   password: <form:input path="password"/><br/>
   <input type="submit"/>
</form:form>

</body></html>